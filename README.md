# **Some (_quick_) analysis notes about TEMU** - _until November 20th, 2023_
**_Last updated :_**
- **_November 24, 2023_**       - _added 2.22.3 & 2.22.6 releases_

\
**TEMU** is an online marketplace, that offers heavily discounted goods from Chinese sellers to consumers abroad.

Such marketplace is owned by PDD Holdings - _a multinational conglomerate which also owns the online commerce platform Pinduoduo in China_ - and operated by its subsidiaries, according to the user's purchase/shipping country.

| Region / Country                 | Subsidiary                             |
|----------------------------------|----------------------------------------|
| United States                    | Whaleco Inc. (_Delaware_)              |
| Canada                           | Whaleco Canada Inc.                    |
| United Kingdom                   | Whaleco UK Limited                     |
| Other supported countries        | Whaleco Technology Limited (_Ireland_) |

\
Users can access Temu platform services via the [official website](https://www.temu.com/) and/or mobile apps - available for both Apple devices with iOS/iPadOS (_9.0 and later_) and Android devices (_Lollipop 5.0 and later_).

This repository contains my static and dynamic analysis results of the TEMU mobile app for Android.

[All officially released TEMU versions for Android are still available for download from the official sources](#11-which-release-should-i-download). Therefore everybody can review both current/recent build releases and past ones of TEMU app for Android - without any need to rely on unofficial/untrustworthy third-party repositories for APKs sample retrieval.

Indeed, I've managed to go back in time not just to Spring 2023, when Temu platform had to re-arrange its privacy/tracking settings for GDPR compliance (_platform expansion to EU countries/users_), but even to Summer 2022 (_see [Appendix I - list of release builds](#7-appendix-i-list-of-release-builds)_).

----------------

### Index

- 1. [Let's download the latest Android release(s) of TEMU app](#1-lets-download-the-latest-android-releases-of-temu-app)

- - 1.1 [Which release should I download?](#11-which-release-should-i-download)

- - 1.2 [Changelog](#12-changelog)

- 2. [Some analysis tips](#2-some-analysis-tips)

- 3. [Let's see now what we've just downloaded](#3-lets-see-now-what-weve-just-downloaded)

- 4. [Start me up!](#4-start-me-up)

- 5. [Privacy Matters & Data Security](#5-privacy-matters-data-security)

- - 5.1. [Permissions & Activities](#51-permissions-activities)

- - 5.2. [Device Info & Tracking](#52-device-info-tracking)

- - 5.3. [Obfuscation & MANWE wrapper/SDK](#53-obfuscation-manwe-wrapper-sdk)

- 6. [Conclusions](#6-conclusions)

- 7. [Appendix I - list of release builds](#7-appendix-i-list-of-release-builds)

- 8. [Appendix II - online analysis references](#8-appendix-ii-online-analysis-references)

----------------
----------------

# 1. Let's download the latest Android release(s) of TEMU app

### 1.1. Which release should I download?

The [download page on TEMU website](https://www.temu.com/download.html) only lists Google Play Store as official source for the Android version of the mobile app.

|**Play Store**|https://play.google.com/store/apps/details?id=com.einnovation.temu|
|--------------|------------------------------------------------------------------|

\
At the time of publishing my notes, the most recent version available from Google Play Store is **2.22.6** (_build 22206_) - released on November 21, 2023.

However, since TEMU developers directly test builds in production environment by releasing public version updates to Google Play Store with [stage rollout](https://support.google.com/googleplay/android-developer/answer/6346149), most users still get previous version 2.22.3 (_build 22203_) as the latest available one for download/update.

\
I've uploaded [some reviewed samples of previous releases](samples) to this repository, but users can still **directly download any TEMU version from Google Play Store** - even the halted staged rollout ones - by just using third-party opensource clients, such as [**Raccoon - PC APK Downloader**](https://github.com/onyxbits/raccoon4) or [**Aurora Store**](https://gitlab.com/AuroraOSS/AuroraStore/).
 
The full list of officially released TEMU versions until November 24, 2023 - _together with their buildcode references_ - is available in the [Appendix I - list of release builds](#7-appendix-i-list-of-release-builds).

----------

\
{- Notice -}

Users with Samsung and Amazon devices may find TEMU releases for Android in their respective vendor app stores too.

|**Vendor App stores**| Direct Link                                               | Latest |
|---------------------|-----------------------------------------------------------|--------|
|Amazon AppStore      |https://www.amazon.com/Whaleco-Inc-Temu/dp/B0BL883JDZ/     | 2.3.1  |
|Samsung Galaxy Store |https://galaxystore.samsung.com/detail/com.einnovation.temu| 2.17.2 |

\
However, 

| **Source**         |Developer's Signature | DEX/res | Libs    |Publisher Details |
|--------------------|----------------------|---------|---------|------------------|
|Amazon AppStore     |{-X-} (1)             |{-X-} (2)|{-X-} (3)|                  |
|Samsung Galaxy Store|                      |{-X-} (2)|         |{-X-} (4)         |

1. **The available version from Amazon AppStore is {- NOT signed with the same developer certificate -} of Google Play Store official releases** (_and Samsung Galaxy Store ones too_). Both fingerprint/serial and issuer/subject are different (_ELEMENTARY INNOVATION PTE LTD instead of whaleco_).

2. DEX Differences

3. Amazon version includes the extra arm64-v8a library `libpvss_tailor.so`

4. Samsung version a phone number `+1 (646) 359-4861` and a street address `293 EISENHOWER PKWY`

- Samsung Technical Support closed the ticket with the following reply

> _"We believe that the TEMU app provided in the Galaxy Store was submitted by TEMU or the rights holder._
>
> _If the TEMU app in the Galaxy Store infringes copyright, TEMU or the rights holder may report it via the copyright infringement reporting site below."_
>
> _https://help.content.samsung.com/csseller/ticket/createCopyrightQuestionTicket.do_




----------------
----------------

### 1.2. Changelog

TEMU doesn't provide a detailed changelog about its mobile app releases. Neither on its official website nor on Google Play Store.

The companion note of all available updates from Google Play Store is indeed a generic _"Bug fixes and Improvements"_.

\


| **Changelog of declared / requested permissions** (_since 1.0.2 up to 2.22.6 release_)      |
|---------------------------------------------------------------------------------------------|
| ![alt text](doc_img/img_12_perms_22201.png "Changelog of declared / requested permissions") |

\
summary table

|**Release Date**|**Version**| **Build**|**Perms**| **Notice**                                 |
|----------------|-----------|----------|---------|--------------------------------------------|
|	Aug 25, 2022 |	1.0.2	 |	10002	|   24    |First Official release from Play Store      |
|	Sep 29, 2022 |	1.8.0	 |	10800	|   30    |Added R/W Sync + G/A/M Account + Alarm perms|
|	Oct 27, 2022 |	1.15.0	 |	11500	|   29    |{- First release using MANWE wrapper/SDK -} |
|	Dec 30, 2022 |	1.33.0	 |	13300	|   30    |Added Read Contacts perm                    |
|	Jan 31, 2023 |	1.42.0	 |	14200	|   31    |Added High Sampling Rate Sensors perm       |
|	Mar 15, 2023 |	1.54.0	 |	15400	|   32    |Added Adjust SDK Preloaded apps query perm  |
|	Mar 20, 2023 |	1.55.2	 |	15502	|   32    |{- Last release using MANWE wrapper/SDK -}  |
|	Apr 6, 2023  |	1.58.1	 |	15801	|   24    |{+Removed Read Contacts & custom OEM perms+}|
|	Apr 14, 2023 |	1.63.0	 |	16300	|   20    |{+Removed BT & Alarm & Sensor & Sync perms+}|
|	Apr 24, 2023 |	1.65.0	 |	16500	|   16    |{+Removed A/M Account & c/f Location perms+}|
|	May 18, 2023 |	1.71.0	 |	17100	|   15    |{+Removed Record Audio perm+}               |
|	May 24, 2023 |	1.73.0	 |	17300	|   14    |{+Removed Camera perm+}                     |
|	May 31, 2023 |	1.74.5	 |	17405	|   12    |{+Removed R/W External Storage perms+}      |
|	Sep 8, 2023  |	2.4.1	 |	20401	|   14    |Reinstated coarse/fine Location perms       |
|   Oct 13, 2023 |  2.11.0   |  21100   |   13    |{+Removed custom TEMU AB config perm+}      |
|**Nov 24, 2023**| **2.22.6**|**22206** | **13**  |**Current latest release from Play Store**  |

----------------
----------------

## 7. Appendix I - list of release builds

- 2023 releases from April onwards

|**Release Date**   |**Version**| **Build** |
|-------------------|-----------|-----------|
|	Nov 24, 2023 	|	2.22.6	|	22206	|
|	Nov 22, 2023 	|	2.22.3	|	22203	|
|	Nov 21, 2023 	|	2.22.1	|	22201	|
|	Nov 17, 2023 	|	2.21.1	|	22101	|
|	Nov 16, 2023 	|	2.20.5	|	22005	|
|	Nov 14, 2023 	|	2.20.0	|	22000	|
|	Nov 12, 2023 	|	2.19.0	|	21900	|
|	Nov 9, 2023 	|	2.18.5	|	21805	|
|	Nov 7, 2023 	|	2.18.0	|	21800	|
|	Nov 5, 2023 	|	2.17.2	|	21702	|
|	Nov 3, 2023 	|	2.17.0	|	21700	|
|	Nov 2, 2023 	|	2.16.6	|	21606	|
|	Oct 31, 2023	|	2.16.0	|	21600	|
|	Oct 30, 2023	|	2.15.2	|	21502	|
|	Oct 27, 2023	|	2.15.1	|	21501	|
|	Oct 26, 2023	|	2.14.5	|	21405	|
|	Oct 24, 2023	|	2.14.0	|	21400	|
|	Oct 22, 2023	|	2.13.1	|	21301	|
|	Oct 19, 2023	|	2.12.7	|	21207	|
|	Oct 18, 2023	|	2.12.1	|	21201	|
|	Oct 17, 2023	|	2.12.0	|	21200	|
|	Oct 14, 2023	|	2.11.1	|	21101	|
|	Oct 13, 2023	|	2.11.0	|	21100	|
|	Oct 12, 2023	|	2.10.5	|	21005	|
|	Oct 10, 2023	|	2.10.0	|	21000	|
|	Oct 8, 2023		|	2.9.5	|	20905	|
|	Oct 6, 2023		|	2.9.2	|	20902	|
|	Sep 28, 2023	|	2.9.1	|	20901	|
|	Sep 26, 2023 	|	2.8.1	|	20801	|
|	Sep 24, 2023 	|	2.7.2	|	20702	|
|	Sep 19, 2023 	|	2.6.2	|	20602	|
|	Sep 15, 2023 	|	2.5.1	|	20501	|
|	Sep 11, 2023 	|	2.4.1	|	20401	|
|	Sep 8, 2023 	|	2.3.1	|	20301	|
|	Sep 4, 2023 	|	2.2.0	|	20200	|
|	Aug 29, 2023 	|	2.0.1	|	20001	|
|	Aug 27, 2023 	|	1.99.1	|	19901	|
|	Aug 21, 2023 	|	1.98.0	|	19800	|
|	Aug 17, 2023 	|	1.97.0	|	19700	|
|	Aug 14, 2023 	|	1.96.1	|	19601	|
|	Aug 11, 2023 	|	1.95.0	|	19500	|
|	Aug 8, 2023 	|	1.94.1	|	19401	|
|	Aug 5, 2023 	|	1.93.0	|	19300	|
|	Jul 31, 2023 	|	1.92.0	|	19200	|
|	Jul 27, 2023 	|	1.91.0	|	19100	|
|	Jul 24, 2023 	|	1.90.0	|	19000	|
|	Jul 23, 2023 	|	1.89.2	|	18902	|
|	Jul 17, 2023 	|	1.88.0	|	18800	|
|	Jul 12, 2023 	|	1.86.1	|	18601	|
|	Jul 5, 2023 	|	1.84.2	|	18402	|
|	Jun 27, 2023 	|	1.80.4	|	18004	|
|	Jun 13, 2023 	|	1.79.1	|	17901	|
|	Jun 5, 2023 	|	1.77.0	|	17700	|
|	May 31, 2023 	|	1.74.5	|	17405	|
|	May 24, 2023 	|	1.73.0	|	17300	|
|	May 18, 2023 	|	1.71.0	|	17100	|
|	May 15, 2023 	|	1.70.1	|	17001	|
|	May 9, 2023 	|	1.69.0	|	16900	|
|	Apr 24, 2023 	|	1.65.0	|	16500	|
|	Apr 14, 2023 	|	1.63.0	|	16300	|
|	Apr 8, 2023 	|	1.61.1	|	16101	|
|	Apr 6, 2023 	|	1.58.1	|	15801	|

- Previous releases using MANWE wrapper/SDK (_between the end of October 2022 and March 2023_)

|**Release Date**   |**Version**| **Build** |
|-------------------|-----------|-----------|
|	Mar 20, 2023 	|	1.55.2	|	15502	|
|	Mar 17, 2023 	|	1.55.0	|	15500	|
|	Mar 15, 2023 	|	1.54.0	|	15400	|
|	Mar 10, 2023 	|	1.53.1	|	15301	|
|	Mar 7, 2023 	|	1.52.0	|	15200	|
|	Mar 5, 2023 	|	1.51.2	|	15102	|
|	Mar 3, 2023 	|	1.51.1	|	15101	|
|	Feb 24, 2023 	|	1.49.1	|	14901	|
|	Feb 21, 2023 	|	1.48.0	|	14800	|
|	Feb 17, 2023 	|	1.47.0	|	14700	|
|	Feb 14, 2023 	|	1.46.1	|	14601	|
|	Feb 12, 2023 	|	1.45.2	|	14502	|
|	Feb 10, 2023 	|	1.45.1	|	14501	|
|	Feb 7, 2023		|	1.44.0	|	14400	|
|	Feb 3, 2023		|	1.43.0	|	14300	|
|	Jan 31, 2023	|	1.42.0	|	14200	|
|	Jan 28, 2023	|	1.41.1	|	14101	|
|	Jan 24, 2023	|	1.40.0	|	14000	|
|	Jan 24, 2023	|	1.39.0	|	13900	|
|	Jan 20, 2023	|	1.38.0	|	13800	|
|	Jan 17, 2023	|	1.37.1	|	13701	|
|	Jan 10, 2023	|	1.36.0	|	13600	|
|	Jan 8, 2023		|	1.35.2	|	13502	|
|	Jan 3, 2023		|	1.34.1	|	13401	|
|	Dec 30, 2022	|	1.33.0	|	13300	|
|	Dec 30, 2022	|	1.32.0	|	13200	|
|	Dec 23, 2022	|	1.31.0	|	13100	|
|	Dec 21, 2022	|	1.30.2	|	13002	|
|	Dec 16, 2022	|	1.29.0	|	12900	|
|	Dec 16, 2022	|	1.28.0	|	12800	|
|	Dec 11, 2022	|	1.27.0	|	12700	|
|	Dec 9, 2022		|	1.26.0	|	12600	|
|	Dec 2, 2022		|	1.25.0	|	12500	|
|	Nov 29, 2022	|	1.24.0	|	12400	|
|	Nov 25, 2022	|	1.23.0	|	12300	|
|	Nov 21, 2022	|	1.22.0	|	12200	|
|	Nov 18, 2022	|	1.21.0	|	12100	|
|	Nov 15, 2022	|	1.20.1	|	12001	|
|	Nov 11, 2022	|	1.19.0	|	11900	|
|	Nov 9, 2022		|	1.18.1	|	11801	|
|	Nov 4, 2022		|	1.17.0	|	11700	|
|	Oct 31, 2022	|	1.16.0	|	11600	|
|	Oct 27, 2022	|	1.15.0	|	11500	|

- Previous 2022 releases - since official launch on August 25th

|**Release Date**   |**Version**| **Build** |
|-------------------|-----------|-----------|
|	Oct 26, 2022	|	1.14.1	|	11401	|
|	Oct 21, 2022	|	1.13.0	|	11300	|
|	Oct 18, 2022	|	1.12.0	|	11200	|
|	Oct 14, 2022	|	1.11.0	|	11100	|
|	Oct 11, 2022	|	1.10.0	|	11000	|
|	Oct 7, 2022		|	1.9.0	|	10900	|
|	Sep 29, 2022	|	1.8.0	|	10800	|
|	Sep 27, 2022	|	1.7.0	|	10700	|
|	Sep 23, 2022	|	1.6.1	|	10601	|
|	Sep 20, 2022	|	1.5.0	|	10500	|
|	Sep 15, 2022	|	1.4.0	|	10400	|
|	Sep 8, 2022		|	1.3.0	|	10300	|
|	Sep 6, 2022		|	1.2.0	|	10200	|
|	Sep 3, 2022		|	1.1.1	|	10101	|
|	Aug 25, 2022	|	1.0.2	|	10002	|

----------------
----------------

## 8. Appendix II - online analysis references

These are some online static-analysis references for the latest 20 TEMU versions, that have been released on Google Play Store - just during the last 40 days.

| Analysis Platform       | **2.22.6 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/764a39dd1f81a43fb0d09f8a17aaef3dd05907baaafe90dc60f7588772b9fdec           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=764a39dd1f81a43fb0d09f8a17aaef3dd05907baaafe90dc60f7588772b9fdec      |
| Pithus     (_base_)     | https://beta.pithus.org/report/764a39dd1f81a43fb0d09f8a17aaef3dd05907baaafe90dc60f7588772b9fdec           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1347443    |

| Analysis Platform       | **2.22.3 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/80a595a49759de128eed26464c0208e440aed28d3aeaae73d1b3f9fb5245b4ef           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=80a595a49759de128eed26464c0208e440aed28d3aeaae73d1b3f9fb5245b4ef      |
| Pithus     (_base_)     | https://beta.pithus.org/report/80a595a49759de128eed26464c0208e440aed28d3aeaae73d1b3f9fb5245b4ef           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1346449    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/93814/         |

| Analysis Platform       | **2.22.1 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/085a27083c8ccd7ef59bf32af0402df38b32710287fcc21c305b4df84733a52b           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=085a27083c8ccd7ef59bf32af0402df38b32710287fcc21c305b4df84733a52b      |
| Pithus     (_base_)     | https://beta.pithus.org/report/085a27083c8ccd7ef59bf32af0402df38b32710287fcc21c305b4df84733a52b           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1345795    |

| Analysis Platform       | **2.21.1 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/ecd230094623a1dd15a9769281a31fb2788f12c69b4f7763d4b16866b2801cce           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=ecd230094623a1dd15a9769281a31fb2788f12c69b4f7763d4b16866b2801cce      |
| Pithus     (_base_)     | https://beta.pithus.org/report/ecd230094623a1dd15a9769281a31fb2788f12c69b4f7763d4b16866b2801cce           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1344249    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/93620/         |

| Analysis Platform       | **2.20.5 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/a3d6707415955061c080fe6fac5bf5ac0beae1279f102784c67457e4ceb9f41a           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=a3d6707415955061c080fe6fac5bf5ac0beae1279f102784c67457e4ceb9f41a      |
| Pithus     (_base_)     | https://beta.pithus.org/report/a3d6707415955061c080fe6fac5bf5ac0beae1279f102784c67457e4ceb9f41a           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1343765    |

| Analysis Platform       | **2.20.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/261556d851033dfe41a7b8c272174b212a223424446035bf38c44631734b0d94           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=261556d851033dfe41a7b8c272174b212a223424446035bf38c44631734b0d94      |
| Pithus     (_base_)     | https://beta.pithus.org/report/261556d851033dfe41a7b8c272174b212a223424446035bf38c44631734b0d94           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1342452    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/93366/         |

| Analysis Platform       | **2.19.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/bf830be38e652fa08684ac6e0921e6fd89a2b5bf6dbbd9f3165021f6bf574c09           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=bf830be38e652fa08684ac6e0921e6fd89a2b5bf6dbbd9f3165021f6bf574c09      |
| Pithus     (_base_)     | https://beta.pithus.org/report/bf830be38e652fa08684ac6e0921e6fd89a2b5bf6dbbd9f3165021f6bf574c09           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1341371    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/93178/         |

| Analysis Platform       | **2.18.5 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/9549b99d28e9d5a9fc288cf29e7e4634ec0b7cc5580ee1e12f95016e4038d0da           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=9549b99d28e9d5a9fc288cf29e7e4634ec0b7cc5580ee1e12f95016e4038d0da      |
| Pithus     (_base_)     | https://beta.pithus.org/report/9549b99d28e9d5a9fc288cf29e7e4634ec0b7cc5580ee1e12f95016e4038d0da           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1339894    |

| Analysis Platform       | **2.18.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/16799e7f33edd93d7195a3413581e43a8dc1821f628c03fd08af9fb0fb1cc6d1           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=16799e7f33edd93d7195a3413581e43a8dc1821f628c03fd08af9fb0fb1cc6d1      |
| Pithus     (_base_)     | https://beta.pithus.org/report/16799e7f33edd93d7195a3413581e43a8dc1821f628c03fd08af9fb0fb1cc6d1           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1338354    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/92970/         |

| Analysis Platform       | **2.17.2 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/0248d1f66fe406775165db24c2385681a6f66e216bc305f3ca8452174592625b           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=0248d1f66fe406775165db24c2385681a6f66e216bc305f3ca8452174592625b      |
| Pithus     (_base_)     | https://beta.pithus.org/report/0248d1f66fe406775165db24c2385681a6f66e216bc305f3ca8452174592625b           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1337259    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/92751/         |

| Analysis Platform       | **2.17.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/e196af6ea6a3ce365e9599cc8b78d75dfa183f100caef4586332b0c2e9bfa25d           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=e196af6ea6a3ce365e9599cc8b78d75dfa183f100caef4586332b0c2e9bfa25d      |
| Pithus     (_base_)     | https://beta.pithus.org/report/e196af6ea6a3ce365e9599cc8b78d75dfa183f100caef4586332b0c2e9bfa25d           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1336875    |

| Analysis Platform       | **2.16.6 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/4a0df6984190ab414e0a2d7ab9a0be949d6ec44aba8d636d5711066c60712aa8           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=4a0df6984190ab414e0a2d7ab9a0be949d6ec44aba8d636d5711066c60712aa8      |
| Pithus     (_base_)     | https://beta.pithus.org/report/4a0df6984190ab414e0a2d7ab9a0be949d6ec44aba8d636d5711066c60712aa8           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1336062    |

| Analysis Platform       | **2.16.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/611571a3ae0bada0bbcbb59deeffc3f71eb0f58811c65a6879609e11e6e37957           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=611571a3ae0bada0bbcbb59deeffc3f71eb0f58811c65a6879609e11e6e37957      |
| Pithus     (_base_)     | https://beta.pithus.org/report/611571a3ae0bada0bbcbb59deeffc3f71eb0f58811c65a6879609e11e6e37957           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1335154    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/92602/         |

| Analysis Platform       | **2.15.2 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/e45ff578cf611d09d82337f6ec62be716bdbbf8247470e2a72f5748450160ef3           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=e45ff578cf611d09d82337f6ec62be716bdbbf8247470e2a72f5748450160ef3      |
| Pithus     (_base_)     | https://beta.pithus.org/report/e45ff578cf611d09d82337f6ec62be716bdbbf8247470e2a72f5748450160ef3           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1335155    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/92380/         |

| Analysis Platform       | **2.15.1 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/35fd0b2a7f8e938faebaf224223e3ee31edd7a2b6da651332ffd3e575a8eb5e2           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=35fd0b2a7f8e938faebaf224223e3ee31edd7a2b6da651332ffd3e575a8eb5e2      |
| Pithus     (_base_)     | https://beta.pithus.org/report/35fd0b2a7f8e938faebaf224223e3ee31edd7a2b6da651332ffd3e575a8eb5e2           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1335156    |

| Analysis Platform       | **2.14.5 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/ea692e33c2d602f08aea9e2c6a60f816b987ed9890ee0b4e82556532f7f83016           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=ea692e33c2d602f08aea9e2c6a60f816b987ed9890ee0b4e82556532f7f83016      |
| Pithus     (_base_)     | https://beta.pithus.org/report/ea692e33c2d602f08aea9e2c6a60f816b987ed9890ee0b4e82556532f7f83016           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1332708    |

| Analysis Platform       | **2.14.0 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/c38f4ca20841c1ae093852c6895bda9df026f0b9923b684f88cfaef9c0a830f6           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=c38f4ca20841c1ae093852c6895bda9df026f0b9923b684f88cfaef9c0a830f6      |
| Pithus     (_base_)     | https://beta.pithus.org/report/c38f4ca20841c1ae093852c6895bda9df026f0b9923b684f88cfaef9c0a830f6           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1332007    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/92005/         |

| Analysis Platform       | **2.13.1 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/b2cdbd6501ac57a78ae1969b8a03058bd7fb0adc92ce05e38f0a47ab1fff620e           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=b2cdbd6501ac57a78ae1969b8a03058bd7fb0adc92ce05e38f0a47ab1fff620e      |
| Pithus     (_base_)     | https://beta.pithus.org/report/b2cdbd6501ac57a78ae1969b8a03058bd7fb0adc92ce05e38f0a47ab1fff620e           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1330072    |

| Analysis Platform       | **2.12.7 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/71382b0e4495355d12e4ef40d48b25c024388b4a00b0d109a6531761e4937057           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=71382b0e4495355d12e4ef40d48b25c024388b4a00b0d109a6531761e4937057      |
| Pithus     (_base_)     | https://beta.pithus.org/report/71382b0e4495355d12e4ef40d48b25c024388b4a00b0d109a6531761e4937057           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1329299    |

| Analysis Platform       | **2.12.1 Reports**                             |
|-------------------------|------------------------------------------------|
| VirusTotal (_base_)     | https://www.virustotal.com/gui/file/52ac6b2b95214bd155d5bb2fd72e2f0a01ef6bdb47eff099915f50dbb095bf3f           |
| ApkLab     (_base_)     | https://apklab.io/apk.html?hash=52ac6b2b95214bd155d5bb2fd72e2f0a01ef6bdb47eff099915f50dbb095bf3f      |
| Pithus     (_base_)     | https://beta.pithus.org/report/52ac6b2b95214bd155d5bb2fd72e2f0a01ef6bdb47eff099915f50dbb095bf3f           |
| JoeSandbox (_base_)     | https://www.joesandbox.com/analysis/1328128    |
| Ostorlab   (_base+libs_)| https://report.ostorlab.co/scan/91514/         |

