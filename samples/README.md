# Samples

Backup copies of the reviewed TEMU versions for this report.

All samples have been directly downloaded from the following sources :

| **Mobile App Store** | **Temu Card/Page**                                                 |
|----------------------|--------------------------------------------------------------------|
| Google Play Store    | https://play.google.com/store/apps/details?id=com.einnovation.temu |
| Amazon AppStore      | https://www.amazon.com/Whaleco-Inc-Temu/dp/B0BL883JDZ/             |
| Samsung Galaxy Store | https://galaxystore.samsung.com/detail/com.einnovation.temu        |

\
You can use **SAI (Split APKs Installer)** to install them on [armeabi-v7a / arm64-v8a](https://developer.android.com/ndk/guides/abis) physical devices and emulated environments.


| **SAI (Split APKs Installer)** | [Github](https://github.com/Aefyr/SAI) | [F-Droid](https://f-droid.org/packages/com.aefyr.sai.fdroid/) | [Google Play Store](https://play.google.com/store/apps/details?id=com.aefyr.sai) |
|--------------------------------|----------------------------------------|--------|-----------|